Calibration results
===================
Normalized Residuals
----------------------------
Reprojection error (cam0):     mean 0.138285617498, median 0.120311646903, std: 0.0880281967915
Gyroscope error (imu0):        mean 0.177421529931, median 0.147320881777, std: 0.11966533915
Accelerometer error (imu0):    mean 0.762288001212, median 0.675597033264, std: 0.476354339146

Residuals
----------------------------
Reprojection error (cam0) [px]:     mean 0.138285617498, median 0.120311646903, std: 0.0880281967915
Gyroscope error (imu0) [rad/s]:     mean 0.00468828948466, median 0.00389289248703, std: 0.00316210637702
Accelerometer error (imu0) [m/s^2]: mean 0.0201054092548, median 0.0178189277852, std: 0.012563885203

Transformation (cam0):
-----------------------
T_ci:  (imu0 to cam0): 
[[ 0.99980625  0.01752372  0.00896505 -0.00887107]
 [-0.01758587  0.99982154  0.00690103 -0.00477752]
 [-0.00884252 -0.00705735  0.999936   -0.00645548]
 [ 0.          0.          0.          1.        ]]

T_ic:  (cam0 to imu0): 
[[ 0.99980625 -0.01758587 -0.00884252  0.00872825]
 [ 0.01752372  0.99982154 -0.00705735  0.00488657]
 [ 0.00896505  0.00690103  0.999936    0.00656757]
 [ 0.          0.          0.          1.        ]]

timeshift cam0 to imu0: [s] (t_imu = t_cam + shift)
-0.000185140570149


Gravity vector in target coords: [m/s^2]
[-0.0376759  -9.54271136 -2.25912888]


Calibration configuration
=========================

cam0
-----
  Camera model: pinhole
  Focal length: [276.7663686797786, 276.68632433201986]
  Principal point: [315.4287981392361, 238.5810935677864]
  Distortion model: equidistant
  Distortion coefficients: [-0.0018883689284674493, 0.0066653231773309675, -0.002257664084860022, -0.0002502052099516597]
  Type: aprilgrid
  Tags: 
    Rows: 6
    Cols: 6
    Size: 0.0827 [m]
    Spacing 0.02481 [m]



IMU configuration
=================

IMU0:
----------------------------
  Model: calibrated
  Update rate: 200.0
  Accelerometer:
    Noise density: 0.001865 
    Noise density (discrete): 0.0263750829383 
    Random walk: 0.0002
  Gyroscope:
    Noise density: 0.0018685
    Noise density (discrete): 0.0264245804129 
    Random walk: 4e-06
  T_i_b
    [[ 1.  0.  0.  0.]
     [ 0.  1.  0.  0.]
     [ 0.  0.  1.  0.]
     [ 0.  0.  0.  1.]]
  time offset with respect to IMU0: 0.0 [s]

