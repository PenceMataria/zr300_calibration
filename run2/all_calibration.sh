#!/bin/bash

BAG_DATE="2017-05-11-18-42-09"
BAG="$BAG_DATE.bag"
echo $BAG

CAM_TOPIC="/falcon2/fisheye/fisheye"

#1. Realsense fish-eye camera intrinsic calibration
echo "==========================================================="
echo "    1. Realsense fish-eye camera intrinsic calibration     "
echo "==========================================================="

kalibr_calibrate_cameras --target ../parameter_files/april_6x6.yaml --bag $BAG --models pinhole-equi --topics $CAM_TOPIC --dont-show-report
mkdir intrinsic
mv *.pdf ./intrinsic
mv *.yaml ./intrinsic
mv *.txt ./intrinsic

#2. Realsense fish-eye camera to realsense IMU extrinsic calibration
echo "==================================================================="
echo "2. Realsense fish-eye camera to realsense IMU extrinsic calibration"
echo "==================================================================="
kalibr_calibrate_imu_camera --cam "./intrinsic/camchain-$BAG_DATE.yaml" --target ../parameter_files/april_6x6.yaml --imu ../parameter_files/imu_realsense_200Hz.yaml --bag $BAG --time-calibration --dont-show-report
mkdir realsense-imu
mv *.pdf ./realsense-imu
mv *.yaml ./realsense-imu
mv *.txt ./realsense-imu

3. Realsense fish-eye camera to M100 IMU extrinsic calibration
echo "==================================================================="
echo "   3. Realsense fish-eye camera to M100 IMU extrinsic calibration  "
echo "==================================================================="
kalibr_calibrate_imu_camera --cam "./intrinsic/camchain-$BAG_DATE.yaml" --target ../parameter_files/april_6x6.yaml --imu ../parameter_files/imu_DJI_100Hz.yaml --bag $BAG --time-calibration --timeoffset-padding 0.05 --dont-show-report
mkdir m100-imu
mv *.pdf ./m100-imu
mv *.yaml ./m100-imu
mv *.txt ./m100-imu

#4. Outputing MSF and ROVIO calibration files.
echo "==================================================================="
echo "       4. Outputing MSF and ROVIO calibration files.               "
echo "      Please change from cam0 to cam2 in                           "
echo "      rovio_test.info and rovio_cam2 file respectively             "
echo "==================================================================="
kalibr_rovio_config --cam "./realsense-imu/camchain-imucam-$BAG_DATE.yaml"

mkdir calibrationResults
mv *.yaml ./calibrationResults
mv *.info ./calibrationResults
kalibr_msf_config --viimu "./realsense-imu/camchain-imucam-$BAG_DATE.yaml" --mavimu "./m100-imu/camchain-imucam-$BAG_DATE.yaml" --out "msf_parameters_falcon2_realsense-$BAG_DATE.yaml"
mv *.yaml ./calibrationResults